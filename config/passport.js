const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const usuarioModel = require('../models/usuario');


passport.use(new localStrategy(

));

passport.serializeUser(function(user, cb){
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
    usuarioModel.findById(id, (err, usuario) =>{
        cb(err,usuario);
    })
});

module.exports = passport;