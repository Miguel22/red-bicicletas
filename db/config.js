const mongoose = require("mongoose");

const urlDb = "mongodb://localhost:27017/red_bicicletas";

mongoose.connect(urlDb, { useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;

db.on("error", () => console.error.bind(console, "MongoDb connection error: "));
db.once("open", () => console.log("Connection is successful!"));

module.exports = db;
