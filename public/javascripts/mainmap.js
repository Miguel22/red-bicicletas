const lat = 12.0856509;
const log = -86.3026186;

var mainmap = L.map("mainmap").setView([lat, log], 10);

L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
  {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
    accessToken:
      "pk.eyJ1IjoibWphY2FtbyIsImEiOiJja2YzMDd2cHAwMzhwMnNxcTBmcmttbDB2In0.JbUfGlZ37aqGYxW6iLTHQA",
  }
).addTo(mainmap);

/*var marker = L.marker([lat, log]).addTo(mainmap);*/

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function (result) {
    result.bicicletas.map(function (bici) {
      var marker = L.marker(bici.ubicacion, { title: bici.code }).addTo(
        mainmap
      );
      marker
        .bindPopup(`<b>Red bicicletas ${bici.code}</b><br>Aquí.`)
        .openPopup();
    });
  },
});
