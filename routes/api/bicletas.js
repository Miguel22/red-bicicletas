const express = require("express");
const router = express.Router();

const bicicletaController = require("../../controllers/api/bicicletaController");

router.post("/", bicicletaController.bicicleta_create);
router.delete("/:id", bicicletaController.bicicleta_delete);
router.put("/:id", bicicletaController.bicicleta_update);
router.get("/", bicicletaController.bicicleta_list);

module.exports = router;
