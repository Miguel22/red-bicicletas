const router = require("express").Router();

const usuarioController =  require('../controllers/usuario');


router.get("/", usuarioController.list);

router.get("/viewCreate", usuarioController.create_view);

router.post("/", usuarioController.create);

router.post("/:id/viewUpdate", usuarioController.update_view);

router.post("/update", usuarioController.update);

router.post("/:id/delete", usuarioController.delete);

module.exports = router;
