var express = require("express");
var router = express.Router();

const bici_controler = require("../controllers/bicicleta");

router.get("/", bici_controler.bicicletaList);
router.get("/create", bici_controler.bicicletaCreateView);
router.post("/create", bici_controler.bicicletaCreate);
router.post("/:id/delete", bici_controler.bicicletaDelete);
router.post("/:id/edit", bici_controler.bicicletaEditView);
router.post("/edit", bici_controler.bicicletaEdit);

module.exports = router;
