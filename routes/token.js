const router = require('express').Router();
const tokenController = require('../controllers/token');

router.get('/:token', tokenController.saveToken)

module.exports = router;