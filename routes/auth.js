var express = require("express");
var router = express.Router();

const auth_controler = require("../controllers/auth");

router.post("/login", auth_controler.login);
router.get("/viewLogin", auth_controler.viewLogin);
router.post("/logout", auth_controler.logout);
//router.post("/viewReset", bici_controler.bicicletaDelete);
//router.post("/:id/edit", bici_controler.bicicletaEditView);
//router.post("/edit", bici_controler.bicicletaEdit);

module.exports = router;