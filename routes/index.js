var express = require("express");
var appRouter = express();

var usuariosRouter = require("./usuarios");
var biciRouter = require("./bicicletas");
var bicicletasApiRouter = require("./api/bicletas");
var homeRouter = require("./home");
const tokenRouter = require('./token');
const authRouter = require('./auth');


//Rutas de Paginas
appRouter.use("/", homeRouter);
appRouter.use("/token", tokenRouter);
appRouter.use("/users", usuariosRouter);
appRouter.use("/bicicletas", biciRouter);
appRouter.use("/auth", authRouter);



//Rutas de API
appRouter.use("/api/bicicletas", bicicletasApiRouter);


module.exports = appRouter;
