const UsuarioModel = require('../models/usuario');
const TokenModel = require('../models/token');

module.exports = {
    saveToken: async (req, res) => {
       try {
        const {token} = req.params;
        
        const dameToken = await TokenModel.findOne({token});
        if(!dameToken) {
            res.status(400).send({type: 'not verified', msg:'Token invalido'});
            return;
        };
        
        const existeUsuario = await UsuarioModel.findById(dameToken._userId); 
        if(!existeUsuario) res.status(400).send({ msg:'Usuario no existe'});

        if(existeUsuario.verificado) {
            res.redirect('/');
            return;
        }
       
        existeUsuario.verificado= true;
        const actualizaUsuario =  await existeUsuario.save();
        if(actualizaUsuario)res.redirect('/'); 
       } catch (error) {
        res.status(500).send({msg:'Ocurrio un error'});
       }       
    }
}