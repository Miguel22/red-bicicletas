const usuarioServices = require('../services/auth.services');
const TokenModel = require('../models/token'); 

module.exports = {
    login: async (req, res) => {
        try {
            const {email, password} =  req.body;            
            const isAuth =  await usuarioServices.authenticate(email, password); 
            isAuth ? res.redirect('/bicicletas') : res.redirect('/');            
        } catch (error) {
            console.log('Error al loguearse');
        }        
    },

    logout: (req, res) => {
        try {
            res.redirect('/');
        } catch (error) {
            console.log('Error en logout');
        }
    },

    reset: (req, res) => {
        try {
            console.log('Resetear el password');
        } catch (error) {
            console.log('Error al resetear el password');
        }
    },

    viewLogin: (req, res) => {        
        try {            
            res.render("auth/login");
        } catch (error) {
            res.render("/index");
        }
    }
}