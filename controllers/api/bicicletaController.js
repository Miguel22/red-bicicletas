"use strict";

const Bicicleta = require("../../models/bicicletas");

exports.bicicleta_list = async (req, res) => {
  res.status(200).json({
    bicicletas: await Bicicleta.allBici(),
    count: Bicicleta.allBici.length,
  });
};

exports.bicicleta_create = (req, res) => {
  const { code, color, modelo, ubicacion } = req.body;
  const bicicleta = new Bicicleta({ code, color, modelo, ubicacion });
  bicicleta.save();
  res.status(200).json({
    msg: "Create whit successful!!",
    bicicleta: bicicleta,
  });
};

exports.bicicleta_delete = async (req, res) => {
  const { id } = req.params;
  await Bicicleta.remuveByCode(id);
  res.status(200).json({ msg: "Delete is successful!!!" });
};

exports.bicicleta_update = (req, res) => {
  const { id } = req.params;
  const { color, modelo, ubicacion } = req.body;
  Bicicleta.edit({ id, color, modelo, ubicacion });
  res.status(200).json({
    msg: "Update whit successful!!",
    bicicleta: bicicleta.findId(id),
  });
};
