const crypto = require('crypto');

const UsuarioModel = require('../models/usuario');
const TokenModel = require('../models/token');

const sendMail = require('../mailer/emails')


module.exports = {
    
    list: async (req, res)=> {
        const listUser = await UsuarioModel.find();        
        listUser? res.render("usuarios/index",{usuarios: listUser}): console.log(listUser);
    },

    create_view: (req, res)=> {
        res.render('usuarios/create',{errors: {}, usuario: new UsuarioModel({})});
    },

    create: async (req, res)=> {
        try {
            const {name, email, password, password_confirm} = req.body;           
        if (password != password_confirm) {
            res.render('usuarios/create', {errors: {password_confirm: {message: 'Password distintos'}}})            
            return;
        }

        const usuarioNew = await UsuarioModel.create({name, email, password});        
        const usuarioSave = await usuarioNew.save();  

        const token = new TokenModel({_userId: usuarioSave._id, token: crypto.randomBytes(16).toString('hex')});
        const tokenSave = await token.save();

       
        const option = {
            from: 'migjach@gmail.com',
            email: usuarioSave.email,
            subject: 'Verificar cuenta',
            token: tokenSave.token
        }        
        const envio = await sendMail(option);
        
        if(!envio) console.log('Problema al enviar Mensaje');

        res.redirect('/users');
        } catch (errors) {
            res.redirect('/users');
        }

    },

    update_view: async (req, res)=> {
        const {id} = req.params;       
        const usuarioUpdate = await UsuarioModel.findById(id);       
        res.render('usuarios/update',{errors: {}, usuario: usuarioUpdate});
    },

    update: async (req, res)=> {
        const {id,name} = req.body;        
        const usuarioUpdate = await UsuarioModel.findByIdAndUpdate(id,{name});      
        res.redirect('/users');
    },

    delete: async (req, res) => {
        const {id} =  req.params;
        await UsuarioModel.findByIdAndDelete(id);
        await TokenModel.deleteOne({_userId:id})
        res.redirect('/users');
    },





}

