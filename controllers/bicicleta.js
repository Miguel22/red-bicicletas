const Bicicleta = require("../models/bicicletas");

const sendMail = require('../mailer/emails');

exports.bicicletaList = async (req, res) => {  
  res.render("bicicletas/index", {
    bicis: await Bicicleta.find({}),
    count: await Bicicleta.count(),
  });
};

exports.bicicletaCreateView = async (req, res) => {
  const id = (await Bicicleta.count()) + 1;
  res.render("bicicletas/create", { id });
};

exports.bicicletaCreate = async (req, res) => {
  const { code, color, modelo, lat, lng } = req.body;
  const bicicleta = await new Bicicleta({
    code,
    color,
    modelo,
    ubicacion: [lat, lng],
  });

  await bicicleta.save();
  res.redirect("/bicicletas");
};

exports.bicicletaDelete = async (req, res) => {
  const { id } = req.params;
  const bicidelete = await Bicicleta.findOneAndDelete({ code: id });
  bicidelete ? res.redirect("/bicicletas") : console.log(bicidelete);
};

exports.bicicletaEdit = async (req, res) => {
  const { code, color, modelo, lat, lng } = req.body;
  const biciUpdate = await Bicicleta.findOneAndUpdate(
    { code },
    { code, color, modelo, ubicacion: [lat, lng] }
  );
  biciUpdate ? res.redirect("/bicicletas") : console.log(biciUpdate);
};

exports.bicicletaEditView = async (req, res) => {
  const { id } = req.params;
  const modificaBci = await Bicicleta.findOne({ code: id });
  res.render("bicicletas/edit", { bici: modificaBci });
};
