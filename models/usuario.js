const { Schema, model } = require("mongoose");
const bcrypt = require('bcrypt');
const Reserva = require("../models/reserva");
const uniqueValidator = require('mongoose-unique-validator');

const saltRounds = 10;



const validateEmail = (email) => {
    const re = /^[^@]+@[^@]+\.[a-zA-Z]{2,}$/;
    return re.test(email);
}

const usuarioSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'El nombre es Obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es requerido'],
    lowercase: true,
    unique: true,
    validate:[validateEmail, 'Por favor ingrese un email valido'],
    match: [/^[^@]+@[^@]+\.[a-zA-Z]{2,}$/]
  },
  password: {
    type: String,
    required: [true, 'El password es obligatorio']
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false
  }

});

usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')){
      this.password = bcrypt.hashSync(this.password,saltRounds );
    }
    next();
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.statics.reserva = async function (userID,biciId, desde, hasta) {
  const reserva = new Reserva({
    usuario: userID,
    bicicleta: biciId,
    desde,
    hasta,
  }); 
  
  return await reserva.save();
};

module.exports = model("Usuario", usuarioSchema);
