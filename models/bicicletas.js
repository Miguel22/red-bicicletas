"use strict";
const LocalStorage = require("node-localstorage").LocalStorage;

const mongoose = require("mongoose");
const { bicicletaEdit } = require("../controllers/bicicleta");
const Schema = mongoose.Schema;

const bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    createIndexes: { type: "2dsphere", sparse: true },
  },
});

bicicletaSchema.statics.createInstance = function (
  code,
  color,
  modelo,
  ubicacion
) {
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion,
  });
};

bicicletaSchema.methods.toString1 = function () {
  return "code: " + this.code + "| color: " + this.color;
};

bicicletaSchema.statics.allBici = async function () {
  return await this.find({});
};

bicicletaSchema.statics.findByCode = function (code) {
  return this.find({ code: code });
};

bicicletaSchema.statics.remuveByCode = function (code) {
  return this.deleteOne({ code: code });
};

bicicletaSchema.statics.add = function (bici) {
  return bici.save();
};

module.exports = mongoose.model("Bicicleta", bicicletaSchema);
