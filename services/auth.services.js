const usuarioModel = require('../models/usuario');
const tokenServices = require('../services/token.services');
const bcrypt = require('bcrypt');

module.exports = {

    authenticate: async (email, password) => {
        try {
            const usuario = await usuarioModel.findOne({email:email}); 
            if (usuario && usuario.verificado && password) {

                const match = await bcrypt.compareSync(password, usuario.password);
                if(!match) return false;

                const isToken = await tokenServices.getToken(usuario._id);                
                return !isToken? await tokenServices.generateToken(usuario._id): true;                

            }else {
                return false;
            }
        } catch (error) {
            console.error(error);
        }
    }    
}




 