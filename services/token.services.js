const crypto = require('crypto');
const TokenModel = require('../models/token');

module.exports = {

    generateToken: async (idUsuario) => {
        try {
            const token = new TokenModel({_userId: idUsuario, token: crypto.randomBytes(16).toString('hex')});
            const tokenSave = await token.save();
            return tokenSave? tokenSave.token:null;
        } catch (error) {
            console.error('Error al generar token');
        }
    },

    getToken: async ( idUsuario ) => {
        try {
            const getToken = await TokenModel.findOne({_userId:idUsuario}) || null;            
            return getToken;
        } catch (error) {
            console.error('Error al obtener token');
        }
    }
}