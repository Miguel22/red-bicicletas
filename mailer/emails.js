const nodemailer = require('nodemailer');

const sendMailEthereal = async (option) => {


    let testAccount = await nodemailer.createTestAccount();


    // Create a SMTP transporter object
    let transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'roslyn.yundt67@ethereal.email',
            pass: 'VUrrDyZNvmaqpqsCGy'
        }
    });

    // Message object
    let message = {
        from: option.from || 'Sender Name <no-replay@redbicicletas.com>',
        to: `Recipient <${option.email}>`,
        subject: option.subject || 'Verificación de cuenta ✔',
        text: 'Verificación de cuenta',
        html: option.html ||  `<p>En este <a href="http://localhost:3000/token/${option.token}"> link </a> puede verificar su cuentas</p>
        `
    };

    if (message.from && message.to && message.subject && message.html) {
        const infoSend = await  transporter.sendMail(message);        
        console.log('Message sent: %s', infoSend.messageId);
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(infoSend));
        return infoSend;
    }else {
        console.log('Error occurred: Message not sent, check the variables');
        return 
    }

    
};


module.exports = sendMailEthereal;