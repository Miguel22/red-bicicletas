const mongoose = require("mongoose");
const Bicicleta = require("../../models/bicicletas");
describe("Test", () => {
  beforeAll(() => {
    const bici = new Bicicleta({
      code: 2,
      color: "roja",
      modelo: "urbana",
      ubicacion: [-34.5, -54.1],
    });
    bici.save();
  });

  beforeEach(() => {
    const mongodb = "mongodb://localhost:27017/red_bicicletas";
    mongoose.connect(mongodb, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = mongoose.connection;
    db.on("error", () =>
      console.error.bind(console, "MongoDb connection error: ")
    );
    db.once("open", () => {
      console.log("Test Connection is successful!");
    });
  });

  afterEach(async () => {
    const success = await Bicicleta.deleteMany({});
    console.log("afterEach", success);
  });

  afterAll(async () => {
    const success = await Bicicleta.deleteMany({});
    console.log("afterAll", success);
  });

  describe("Bicicleta.delete", () => {
    it("Eliminar una Bicicleta", async () => {
      const bicicletaValida = await Bicicleta.find();
      expect(bicicletaValida.length).toBe(1);
      const bicicleta = await Bicicleta.deleteOne({ code: 2 });
      expect(bicicleta.deletedCount).toBe(1);
    });
  });

  describe("Bicicleta.Create", () => {
    it("Crear una instancia de Bicicleta", async () => {
      const bici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
        ubicacion: [-34.5, -54.1],
      });

      const bicicleta = await bici.save();

      expect(bicicleta.code).toBe(1);
      expect(bicicleta.color).toBe("verde");
      expect(bicicleta.modelo).toBe("urbana");
      expect(bicicleta.ubicacion[0]).toEqual(-34.5);
      expect(bicicleta.ubicacion[1]).toEqual(-54.1);
    });
  });

  describe("Bicicleta.allBici", () => {
    it("comienza vacia", async () => {
      const bici = await Bicicleta.find();
      expect(bici.length).toBe(0);
    });
  });

  describe("Bicicleta.findOne", () => {
    it("Buscar una Bicicleta", async () => {
      Bicicleta.find((err, data) => {
        expect(data.length).toBe(0);
      });

      const bici = new Bicicleta({
        code: 1,
        color: "azul",
        modelo: "urbana",
        ubicacion: [-34.5, -54.1],
      });

      const bici1 = new Bicicleta({
        code: 2,
        color: "roja",
        modelo: "urbana",
        ubicacion: [-34.5, -54.1],
      });

      bici.save();
      bici1.save();

      const bicicleta = await Bicicleta.findOne({ code: 1 });
      expect(bicicleta.code).toBe(1);
    });
  });
});
