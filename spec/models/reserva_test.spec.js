const mongoose = require("mongoose");
const Reserva = require("../../models/reserva");
const Bicicleta = require("../../models/bicicletas");
const Usuario = require("../../models/usuario");

describe("Test Modelo Reserva", () => {
  beforeEach(() => {
    const mongodb = "mongodb://localhost:27017/red_bicicletas";
    mongoose.connect(mongodb, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = mongoose.connection;
    db.on("error", () =>
      console.error.bind(console, "MongoDb connection error: ")
    );
    db.once("open", () => {
      console.log("Test Connection is successful!");
    });
  });

  afterEach(async () => {
    await Usuario.deleteMany({});
    await Bicicleta.deleteMany({});
    await Reserva.deleteMany({});
  });

  describe("Reserva.Create", () => {
    it("Agregar", async () => {
      const user = new Usuario({ name: "Miguel" });     
      const userNew = await user.save();           
      expect((await Usuario.find()).length).toBe(1);

      const bicicleta = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana",
        ubicacion: [-34.5, -54.1],
      });
      await bicicleta.save();      
      expect((await Bicicleta.find()).length).toBe(1);

      

      const desde = new Date();
      const mañana = new Date();
      mañana.setDate(desde.getDate() + 1);
      const reservaNew = await Usuario.reserva( userNew._id, bicicleta._id, desde, mañana);
     
    });
  });
});
