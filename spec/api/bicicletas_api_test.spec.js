const mongoose = require("mongoose");
const Bicicleta = require("../../models/bicicletas");

const request = require("request");
const server = require("../../bin/www");

const url = "http://localhost:3000/api/bicicletas";

describe("Bicicletas API", () => {
  beforeEach(async () => {
    const mongodb = "mongodb://localhost:27017/red_bicicletas";
    mongoose.connect(mongodb, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = mongoose.connection;
    db.on("error", () =>
      console.error.bind(console, "MongoDb connection error: ")
    );
    db.once("open", () => {
      console.log("Test Connection is successful!");
    });
    await Bicicleta.deleteMany({});
  });

  afterEach(async () => await Bicicleta.deleteMany({}));

  afterAll(async () => await Bicicleta.deleteMany({}));

  describe("POST BICICLETAS", () => {
    it("Status 200", async () => {
      const listBici = await Bicicleta.find();
      expect(listBici.length).toBe(0);

      const header = { "content-type": "application/json" };

      const nueva = `{
        "code": 1,
        "color": "Celeste",
        "modelo": "Xixo",
        "ubicacion": [12.0227203,-86.2137992]        
    }`;
      request.post(
        { headers: header, url, body: nueva },
        (error, response, body) => {
          const data = JSON.parse(body);
          expect(data.bicicleta.code).toBe(1);
          expect(response.statusCode).toBe(200);
        }
      );
    });
  });

  describe("GET BICICLETAS", () => {
    it("Status 200", async () => {
      const listBici = await Bicicleta.allBici();
      expect(listBici.length).toBe(0);

      const nueva = new Bicicleta({
        code: 2,
        color: "Azul",
        modelo: "Air",
        ubicacion: [1, 3],
      });
      const nueva2 = new Bicicleta({
        code: 3,
        color: "Rojo",
        modelo: "Air",
        ubicacion: [1, 3],
      });
      nueva.save();
      nueva2.save();
      request.get(url, (error, response, body) => {
        expect(response.statusCode).toBe(200);
      });
    });
  });

  describe("DELETE BICICLETAS", () => {
    it("Status 200", async () => {
      const listBici = await Bicicleta.allBici();
      expect(listBici.length).toBe(0);
      const nueva = new Bicicleta({
        code: 1,
        color: "Azul",
        modelo: "Air",
        ubicacion: [1, 3],
      });
      nueva.save();
      request.del(`${url}/1`, (error, response, body) => {
        expect(response.statusCode).toBe(200);
      });
    });
  });
});
