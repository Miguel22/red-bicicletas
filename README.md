# README.MD

Poryecto web para establecer punto de red de bicicletas.

## Contenido

Este proyecto tiene una arquitectura cliente servidor, api rest, contiene los distintos commit de cada sección de las clases.

## Demostración

Actualmente en desarrollo y en espera de depĺoy.

## Como clonar

- Ingresar a Git desde tu terminal.
- Copiar el siguente enlace: git clone https://bitbucket.org/Miguel22/red-bicicletas.git

## Installation

Para instalar y ejecutar este proyecto, simplemente escriba y ejecute:

```bash
npm install
npm start:dev
```

## Vista previa

Insert here an image of the preview if your project has one. The image can be into the project, you have to indicate the route and look like this.

![](./public/img/home.png)

### Notas

Este es el primer ejercicio de la clases correspondiente al modulo 1
